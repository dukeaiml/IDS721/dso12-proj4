use lambda_runtime::{run, service_fn, Error, LambdaEvent};

use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Request {
    password: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    payload: bool,
    reasons: Vec<String>,
    input_text: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract useful information from the request

    let password = event.payload.password;
    // let input_text = if name == "generate" {
    // //     "Polo".to_string()
    // // } else {
    // //     "Nobody".to_string()
    // // };
    // let input_text = event.payload.name;

    tracing::info!("name {}", password);
    let mut reasons = Vec::new();

    if password.len() < 8 {
        reasons.push("have less than 8 characters".to_string());
    }

    if !password.chars().any(|c| c.is_lowercase()) {
        reasons.push("contain any lowercase letters".to_string());
    }

    if !password.chars().any(|c| c.is_uppercase()) {
        reasons.push("contain any uppercase letters".to_string());
    }

    if !password.chars().any(|c| c.is_digit(10)) {
        reasons.push("contain any digits".to_string());
    }

    let special_characters = "!@#$%^&*()-+`'~{}[]:;\"<>,.?/|\\_";
    if !password.chars().any(|c| special_characters.contains(c)) {
        reasons.push("contain any special characters".to_string());
    }

    for window in password.chars().collect::<Vec<_>>().windows(2) {
        if window[0] == window[1] {
            reasons.push("contain two of the different character in adjacent positions".to_string());
            break;
        }
    }

    let payload_body = if reasons.is_empty() {
        true
    } else {
        false
    };

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        payload: payload_body,
        reasons: reasons,
        input_text: password,
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
