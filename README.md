# Rust AWS Lambda and Step Functions

This project implements a serverless application using AWS Lambda, AWS Step Functions, and Rust programming language. The application checks the strength of a password provided by the user. If the password is strong enough, it proceeds with the input password. Otherwise, it generates a new password and provides reasons for the password's weakness.

## Requirements

* Rust programming language
* AWS account
* AWS CLI installed and configured
* Rust lambda_runtime and tracing_subscriber crates installed

## Project Structure

* check-validility: First Lambda function that checks the validity of the input password.
* password-generator: Second Lambda function that generates a new password if the input password is weak.
* Makefile: Makefile containing commands for building, formatting, testing, and deploying Lambda functions.

## How it Works

### Lambda A (Password Strength Checker):
* Receives the input password from the user.
* Checks the strength of the password based on various criteria such as length, character types, etc.
* If the password is strong enough, it proceeds with the input password. Otherwise, it sends the reasons for the password's weakness to Lambda B.

### Lambda B (Password Generator):
* Receives the reasons for the password's weakness from Lambda A.
* Generates a new password that meets the strength criteria.
* Returns the new password along with a message indicating that a new password has been generated.

### Step Functions State Machine:
* Orchestrates the flow between Lambda A and Lambda B.
* Invokes Lambda A first to check the password strength.
* If the password is strong enough, the state machine proceeds to the final state.
* If the password is weak, the state machine invokes Lambda B to generate a new password and proceeds to the final state.

![](step.png)

## API
I exposed an API endpoint for invoking the Step Functions state machine. API endpoint: https://gr99xfkpwe.execute-api.us-east-1.amazonaws.com/alpha/execution

```
curl -X POST -d '{
    "input": "{\"password\": \" 9e`iPKJ\"}",
    "name": "MyExecution11",
    "stateMachineArn": "arn:aws:states:us-east-1:381491904783:stateMachine:PasswordStepFunctionPipeline"
}' https://gr99xfkpwe.execute-api.us-east-1.amazonaws.com/alpha/execution
```
![](api.png)

![](exec.png)


## Deployment

* Clone this repository:
```
git clone <repository_url>
cd <repository_name>
```

* Build, test and Deploy the Rust code for each Lambda functions:
```
make release
make invoke
make deploy
```


Once deployed, you can invoke the Step Functions state machine using an API endpoint, AWS SDK, or AWS CLI. Provide the input password in the format expected by the state machine, and it will return either the input password (if strong enough) or a newly generated password along with a message indicating the action taken.