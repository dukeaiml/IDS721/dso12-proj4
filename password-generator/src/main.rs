use lambda_runtime::{run, service_fn, Error, LambdaEvent};

use serde::{Deserialize, Serialize};
use passwords::PasswordGenerator;

#[derive(Deserialize)]
struct Request {
    input_text: String,
    reasons: Vec<String>,
    payload: bool,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    password: String,
    condition: String
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract useful information from the request
    let status = event.payload.payload;
    let reason = event.payload.reasons;
    let input_text = event.payload.input_text;

    let condition = if status == true {
        "Password provided is good!".to_string()
    } else {
        "New password has been generated becuase provided password did not ".to_string() + &reason.join(", ")
    };


    let password = if reason.is_empty() {
        input_text.clone()
    } else {
        PasswordGenerator {
            length: 8,
            numbers: true,
            lowercase_letters: true,
            uppercase_letters: true,
            symbols: true,
            spaces: true,
            exclude_similar_characters: false,
            strict: true,
        }.generate_one().unwrap().clone()
    };

    //tracing::info!("name {}", name);

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        password: password,
        condition: condition
    };

    Ok(resp)
}


#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
